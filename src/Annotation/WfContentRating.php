<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Annotation\WfContentRating.
 */

namespace Drupal\sxt_wfrating\Annotation;

/**
 * Defines .... Rating annotation object.
 *
 * @Annotation
 */
class WfContentRating extends WfRatingAnnotationBase {

}
