<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Annotation\WfContentValidator.
 */

namespace Drupal\sxt_wfrating\Annotation;

/**
 * Defines a Plugin annotation object for content validator plugins.
 *
 * @Annotation
 */
class WfContentValidator extends WfRatingAnnotationBase {

}
