<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Annotation\WfRatingAnnotationBase.
 */

namespace Drupal\sxt_wfrating\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a base class for wf plugins ().
 *
 * @Annotation
 */
class WfRatingAnnotationBase extends Plugin {
  
  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the provider that owns the plugin.
   *
   * @var string
   */
  public $provider;

  /**
   * The human-readable name of the plugin.
   *
   * This is used as an administrative summary of what the plugin does.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * Additional administrative information about the plugin's behavior.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation (optional)
   */
  public $description = '';

  /**
   * A default weight for the plugin in new xtsf entity.
   *
   * @var int (optional)
   */
  public $weight = 0;

  /**
   * Whether this plugin is enabled or disabled by default.
   *
   * @var bool (optional)
   */
  public $status = FALSE;

  /**
   * The default settings for the plugin.
   *
   * @var array (optional)
   */
  public $settings = [];
  
}
