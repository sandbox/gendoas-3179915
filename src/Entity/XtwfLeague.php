<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Entity\XtwfLeague.
 */

namespace Drupal\sxt_wfrating\Entity;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Xtwf League configuration entity.
 * 
 * @ConfigEntityType(
 *   id = "xtwfleague",
 *   label = @Translation("Xtwf League"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_wfrating\XtwfLeagueListBuilder",
 *     "form" = {
 *       "add" = "Drupal\sxt_wfrating\Form\XtwfLeagueAddForm",
 *       "edit" = "Drupal\sxt_wfrating\Form\XtwfLeagueEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *       "enable" = "Drupal\sxt_wfrating\Form\XtwfLeagueEnableForm",
 *     },
 *   },
 *   admin_permission = "administer xtwfrating",
 *   config_prefix = "xtwfleague",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/slogsys/wfleague/add",
 *     "edit-form" = "/admin/slogsys/wfleague/manage/{xtwfleague}/edit",
 *     "delete-form" = "/admin/slogsys/wfleague/manage/{xtwfleague}/delete",
 *     "enable-form" = "/admin/slogsys/wfleague/manage/{xtwfleague}/enable",
 *     "collection" = "/admin/slogsys/wfleague/collection",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "weight",
 *   },
 * )
 */
class XtwfLeague extends ConfigEntityBase {

  /**
   * Unique machine label of the league.
   *
   * @var string
   */
  protected $id;

  /**
   * Unique index of the league.
   *
   * @var integer
   */
  protected $league_idx;

  /**
   * The human-readable label of the league.
   *
   * @var string
   */
  protected $label;

  /**
   * Weight of this league.
   *
   * @var int
   */
  protected $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getLeagueIndex() {
    return (integer) $this->league_idx;
  }

  /**
   * {@inheritdoc}
   */
  public function setLeagueIndex(int $index) {
    $this->league_idx = $index;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return $this->status();
  }

  /**
   * {@inheritdoc}
   */
  public function labelCamelCase() {
    return SlogXt::toCamelCase($this->label());
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if ($operation === 'delete') {
      $league_idx = $this->getLeagueIndex();
      if (!$this->isEnabled()) {
        $can_delete = TRUE;
      }
      elseif (!$league_idx) {
        $can_delete = FALSE;
      } else {
        $can_delete = SlogXtwfRating::canDeleteLeague($league_idx);
      }

      if ($return_as_object) {
        return $can_delete ? AccessResult::allowed() : AccessResult::forbidden();
      }
      return $can_delete;
    }

    return parent::access($operation, $account, $return_as_object);
  }

}
