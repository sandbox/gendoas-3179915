<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Entity\XtwfRating.
 */

namespace Drupal\sxt_wfrating\Entity;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_wfrating\WfRatingPluginCollection;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the Xtwf Rating configuration entity.
 * 
 * The entity contains a set of rating plugins, that are presented 
 * to the user to rate.
 *
 * @ConfigEntityType(
 *   id = "xtwfrating",
 *   label = @Translation("Xtwf Rating"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_wfrating\XtwfEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\sxt_wfrating\Form\XtwfRatingAddForm",
 *       "edit" = "Drupal\sxt_wfrating\Form\XtwfRatingEditForm",
 *       "delete" = "Drupal\sxt_wfrating\Form\XtwfEntityDeleteConfirm"
 *     },
 *   },
 *   bundle_entity_type = "xtwfrating",
 *   bundle_of = "xtwfrating",
 *   admin_permission = "administer xtwfrating",
 *   config_prefix = "xtwfrating",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/slogsys/wfrating/add",
 *     "edit-form" = "/admin/slogsys/wfrating/manage/{xtwfrating}/edit",
 *     "delete-form" = "/admin/slogsys/wfrating/manage/{xtwfrating}/delete",
 *     "collection" = "/admin/slogsys/wfrating/collection",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "wfratings",
 *   },
 * )
 */
class XtwfRating extends ConfigEntityBundleBase implements EntityWithPluginCollectionInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var int
   */
  protected $weight = 0;

  /**
   * Configured wfratings for this text format.
   *
   * An associative array of wfratings assigned to the content rating, keyed by the
   * instance ID of each wfrating and using the properties:
   * - id: The plugin ID of the wfrating plugin instance.
   * - provider: The label of the provider that owns the wfrating.
   * - status: (optional) A Boolean indicating whether the wfrating is
   *   enabled in the content rating. Defaults to FALSE.
   * - weight: (optional) The weight of the wfrating in the content rating. Defaults
   *   to 0.
   * - settings: (optional) An array of configured settings for the wfrating.
   *
   * Use XtwfRating::wfratings() to access the actual wfratings.
   *
   * @var array
   */
  protected $wfratings = [];

  /**
   * Holds the collection of wfratings that are attached to this format.
   *
   * @var \Drupal\sxt_wfrating\WfRatingPluginCollection
   */
  protected $wfratingCollection;
  protected $wfratingCollectionAll;


  /**
   * {@inheritdoc}
   */
  public function labelCamelCase() {
    return SlogXt::toCamelCase($this->label());
  }

  /**
   * @return \Drupal\sxt_wfrating\WfRatingPluginCollection
   */
  public function wfratings($instance_id = NULL) {
    if (!isset($this->wfratingCollection)) {
      $manager = SlogXtwfRating::contentRatingManager();
      $this->wfratingCollection = new WfRatingPluginCollection($manager, $this->wfratings);
      $this->wfratingCollection->sort();
    }
    if (isset($instance_id)) {
      return $this->wfratingCollection->get($instance_id);
    }
    return $this->wfratingCollection;
  }

  /**
   */
  public function getPluginsAll() {
    if (!isset($this->wfratingCollectionAll)) {
      $manager = SlogXtwfRating::contentRatingManager();
      $this->wfratingCollectionAll = new WfRatingPluginCollection($manager, $this->wfratings);
      $this->wfratingCollectionAll->getAll();
    }

    return $this->wfratingCollectionAll;
  }
  
  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['wfratings' => $this->wfratings()];
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfig($instance_id, array $configuration) {
    if (!empty($configuration['status'])) {
      $this->wfratings[$instance_id] = $configuration;
      if (isset($this->wfratingCollection)) {
        $this->wfratingCollection->setInstanceConfiguration($instance_id, $configuration);
      }
    }
    else {
      unset($this->wfratings[$instance_id]);
      if (isset($this->wfratingCollection)) {
        $this->wfratingCollection->removeInstanceId($instance_id);
      }
    }
    return $this;
  }

}
