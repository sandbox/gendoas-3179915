<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Entity\XtwfValidator.
 */

namespace Drupal\sxt_wfrating\Entity;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\sxt_wfrating\WfValidatorPluginCollection;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the Xtwf Validator configuration entity.
 * 
 * The entity contains a set of validator plugins, that are 
 * to be applied before sharing a content.
 *
 * @ConfigEntityType(
 *   id = "xtwfvalidator",
 *   label = @Translation("Xtwf Validator"),
 *   handlers = {
 *     "list_builder" = "Drupal\sxt_wfrating\XtwfEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\sxt_wfrating\Form\XtwfValidatorAddForm",
 *       "edit" = "Drupal\sxt_wfrating\Form\XtwfValidatorEditForm",
 *       "delete" = "Drupal\sxt_wfrating\Form\XtwfEntityDeleteConfirm"
 *     },
 *   },
 *   bundle_entity_type = "xtwfvalidator",
 *   bundle_of = "xtwfvalidator",
 *   admin_permission = "administer xtwfrating",
 *   config_prefix = "xtwfvalidator",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight"
 *   },
 *   links = {
 *     "add-form" = "/admin/slogsys/wfvalidator/add",
 *     "edit-form" = "/admin/slogsys/wfvalidator/manage/{xtwfvalidator}/edit",
 *     "delete-form" = "/admin/slogsys/xtwfvalidator/manage/{xtwfvalidator}/delete",
 *     "collection" = "/admin/slogsys/wfvalidator/collection",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "wfvalidators",
 *   },
 * )
 */
class XtwfValidator extends ConfigEntityBundleBase implements EntityWithPluginCollectionInterface {

  /**
   * @var string
   */
  protected $id;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var int
   */
  protected $weight = 0;

  /**
   * Configured wfvalidators for this text format.
   *
   * An associative array of wfvalidators assigned to the content validator, keyed by the
   * instance ID of each wfvalidator and using the properties:
   * - id: The plugin ID of the wfvalidator plugin instance.
   * - provider: The label of the provider that owns the wfvalidator.
   * - status: (optional) A Boolean indicating whether the wfvalidator is
   *   enabled in the content validator. Defaults to FALSE.
   * - weight: (optional) The weight of the wfvalidator in the content validator. Defaults
   *   to 0.
   * - settings: (optional) An array of configured settings for the wfvalidator.
   *
   * Use XtwfValidator::wfvalidators() to access the actual wfvalidators.
   *
   * @var array
   */
  protected $wfvalidators = [];

  /**
   * Holds the collection of wfvalidators that are attached to this format.
   *
   * @var \Drupal\sxt_wfrating\WfValidatorPluginCollection
   */
  protected $wfvalidatorCollection;
  protected $wfvalidatorCollectionAll;

  /**
   * {@inheritdoc}
   */
  public function labelCamelCase() {
    return SlogXt::toCamelCase($this->label());
  }

  /**
   * @return \Drupal\sxt_wfrating\WfValidatorPluginCollection
   */
  public function wfvalidators($instance_id = NULL) {
    if (!isset($this->wfvalidatorCollection)) {
      $manager = SlogXtwfRating::contentValidatorManager();
      $this->wfvalidatorCollection = new WfValidatorPluginCollection($manager, $this->wfvalidators);
      $this->wfvalidatorCollection->sort();
    }
    if (isset($instance_id)) {
      return $this->wfvalidatorCollection->get($instance_id);
    }
    return $this->wfvalidatorCollection;
  }

  /**
   */
  public function getPluginsAll() {
    if (!isset($this->wfvalidatorCollectionAll)) {
      $manager = SlogXtwfRating::contentValidatorManager();
      $this->wfvalidatorCollectionAll = new WfValidatorPluginCollection($manager, $this->wfvalidators);
      $this->wfvalidatorCollectionAll->getAll();  // is loading all
    }

    return $this->wfvalidatorCollectionAll;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['wfvalidators' => $this->wfvalidators()];
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginConfig($instance_id, array $configuration) {
    if (!empty($configuration['status'])) {
      $this->wfvalidators[$instance_id] = $configuration;
      if (isset($this->wfvalidatorCollection)) {
        $this->wfvalidatorCollection->setInstanceConfiguration($instance_id, $configuration);
      }
    } else {
      unset($this->wfvalidators[$instance_id]);
      if (isset($this->wfvalidatorCollection)) {
        $this->wfvalidatorCollection->removeInstanceId($instance_id);
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(string $html_text, $league_idx) {
    $result = [
        'valid' => [],
        'unvalid' => [],
    ];

    // remove header
    preg_match('|(<header>.*</header>)|s', $html_text, $match);
    if ($match && strpos($match[0], 'node__title')) {
      $html_text = str_replace($match[0], '', $html_text);
    }
    // remove mw-toc-heading
    $html_text = preg_replace('|(<h2 id=\"mw-toc-heading\">.*</h2>)|', '', $html_text);

    $num_validated = 0;
    foreach ($this->wfvalidators() as $key => $wfvalidator) {
      if ($wfvalidator->status) {
        $document = Html::load($html_text);
        $wfresult = $wfvalidator->validate($document, $league_idx);
        if (!is_array($wfresult) || !isset($wfresult['valid']) || !isset($wfresult['message'])) {
          $msg = t('Unvalid validate result: @pluginid', ['@pluginid' => $wfvalidator->getPluginId()]);
          throw new \Exception($msg);
        }

        $resultkey = (boolean) $wfresult['valid'] ? 'valid' : 'unvalid';
        $result[$resultkey][] = (string) $wfresult['message'];
        $num_validated++;
      }
    }

    $result['num_validated'] = $num_validated;
    $result['is_valid'] = empty($result['unvalid']);
    return $result;
  }

}
