<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Event\XtwfRatingEvents.
 */

namespace Drupal\sxt_wfrating\Event;

/**
 * Defines events thrown by sxt_wfrating.
 */
final class XtwfRatingEvents {

  /**
   * 
   */
  const SXT_WFRATING_CAN_DELETE_LEAGUE = 'sxt_wfrating.canDeleteLeague';

}
