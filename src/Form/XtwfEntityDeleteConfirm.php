<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfEntityDeleteConfirm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Render\Element;
use Drupal\Core\Entity\EntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 */
class XtwfEntityDeleteConfirm extends EntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $build = parent::buildForm($form, $form_state);
    if (Element::children($build['entity_deletes'])) {
      $build['actions']['submit']['#disabled'] = TRUE;
    }
    
    return $build;
  }

}
