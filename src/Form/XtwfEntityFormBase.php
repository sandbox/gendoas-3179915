<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfEntityFormBase.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a base form for a ...
 */
abstract class XtwfEntityFormBase extends EntityForm {

  abstract protected function verifyWfPlugins($wf_plugins);

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $xtwf_entity = $this->entity;

    $form['#tree'] = TRUE;
    // use filter.admin.js, not own js
    // requires ids: "filters-status-wrapper", 'filter-order'
    $form['#attached']['library'][] = 'filter/drupal.filter.admin';

    $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => $xtwf_entity->label(),
        '#required' => TRUE,
        '#weight' => -30,
    ];
    $form['id'] = [
        '#type' => 'machine_name',
        '#required' => TRUE,
        '#default_value' => $xtwf_entity->id(),
        '#maxlength' => 255,
        '#machine_name' => [
            'exists' => [$this, 'exists'],
            'source' => ['label'],
        ],
        '#disabled' => !$xtwf_entity->isNew(),
        '#weight' => -20,
    ];

    //
    $wf_plugins = $xtwf_entity->getPluginsAll();
    $this->verifyWfPlugins($wf_plugins);

    // Plugin status.
    $form['wfplugins']['status'] = [
        '#type' => 'item',
        '#title' => $this->t('Enabled plugins'),
        // For filter.admin.js
        '#prefix' => '<div id="filters-status-wrapper">',
        '#suffix' => '</div>',
        // This item is used as a pure wrapping container with heading. Ignore its
        // value, since 'wfplugins' should only contain plugin definitions.
        '#input' => FALSE,
    ];
    // Plugin order (tabledrag).
    $form['wfplugins']['order'] = [
        '#type' => 'table',
        // For filter.admin.js
        '#attributes' => ['id' => 'filter-order'],
        '#title' => $this->t('Plugin processing order'),
        '#tabledrag' => [
            [
                'action' => 'order',
                'relationship' => 'sibling',
                'group' => 'wfplugin-order-weight',
            ],
        ],
        '#tree' => FALSE,
        '#input' => FALSE,
        '#theme_wrappers' => ['form_element'],
    ];
    // Plugin settings.
    $form['plugin_settings'] = [
        '#type' => 'vertical_tabs',
        '#title' => $this->t('Plugin settings'),
    ];

    foreach ($wf_plugins as $plugin_id => $wf_plugin) {
      $status = $wf_plugin->status;
      $weight = $wf_plugin->weight;
      $form['wfplugins']['status'][$plugin_id] = [
          '#type' => 'checkbox',
          '#title' => $wf_plugin->getLabel(),
          '#default_value' => $status,
          '#parents' => ['wfplugins', $plugin_id, 'status'],
          '#description' => $wf_plugin->getDescription(),
          '#weight' => $weight,
      ];

      $form['wfplugins']['order'][$plugin_id]['#attributes']['class'][] = 'draggable';
      $form['wfplugins']['order'][$plugin_id]['#weight'] = $weight;
      $form['wfplugins']['order'][$plugin_id]['wfplugin'] = [
          '#markup' => $wf_plugin->getLabel(),
      ];

      $form['wfplugins']['order'][$plugin_id]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => $wf_plugin->getLabel()]),
          '#title_display' => 'invisible',
          '#delta' => 50,
          '#default_value' => $weight,
          '#parents' => ['wfplugins', $plugin_id, 'weight'],
          '#attributes' => ['class' => ['wfplugin-order-weight']],
      ];

      // Retrieve the settings form of the wf plugin. The plugin should not be
      // aware of the xtwf entity. Therefore, it only receives a set of minimal
      // base properties to allow advanced implementations to work.
      $settings_form = [
          '#parents' => ['wfplugins', $plugin_id, 'settings'],
          '#tree' => TRUE,
      ];
      $settings_form = $wf_plugin->settingsForm($settings_form, $form_state);
      if (!empty($settings_form)) {
        $form['wfplugins']['settings'][$plugin_id] = [
            '#type' => 'details',
            '#title' => $wf_plugin->getLabel(),
            '#open' => TRUE,
            '#weight' => $weight,
            '#parents' => ['wfplugins', $plugin_id, 'settings'],
            '#group' => 'plugin_settings',
        ];
        $form['wfplugins']['settings'][$plugin_id] += $settings_form;
      }
    }

    return parent::form($form, $form_state);
  }

  /**
   * Determines if the entity already exists.
   *
   * @param string $xtwfentity_id
   *   The entity ID
   *
   * @return bool
   *   TRUE if the entiy exists, FALSE otherwise.
   */
  public function exists($xtwfentity_id) {
    return (bool) $this->entityTypeManager
                    ->getStorage($this->getEntityType())
                    ->getQuery()
                    ->accessCheck(FALSE)
                    ->condition('id', $xtwfentity_id)
                    ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // @todo Move trimming upstream.
    $entity_id = trim($form_state->getValue('id'));
    $entity_label = trim($form_state->getValue('label'));

    // Ensure that the values to be saved later are exactly the ones validated.
    $form_state->setValueForElement($form['id'], $entity_id);
    $form_state->setValueForElement($form['label'], $entity_label);

    $format_exists = $this->entityTypeManager
            ->getStorage($this->getEntityType())
            ->getQuery()
            ->accessCheck(FALSE)
            ->condition('id', $entity_id, '<>')
            ->condition('label', $entity_label)
            ->execute();
    if ($format_exists) {
      $form_state->setErrorByName('label', t('Entity names must be unique. A entity named %label already exists.', ['%label' => $entity_label]));
    }

    // validate by plugin
    foreach ($this->entity->getPluginsAll() as $wf_plugin) {
      $wf_plugin->validateForm($form, $form_state);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Add the submitted form values to the xtwf entity, and save it.
    $xtwf_entity = $this->entity;
    foreach ($form_state->getValues() as $key => $value) {
      if ($key !== 'wfplugins') {
        $xtwf_entity->set($key, $value);
      } else {
        foreach ($value as $instance_id => $config) {
          if (!empty($config['status']) && !empty($config['settings']['leagues'])) {
            $leagues = &$config['settings']['leagues'];
            foreach ($leagues as $league_id => $league) {
              if (empty($league['override'])) {
                unset($leagues[$league_id]);
              }
            }
            if (empty($config['settings']['leagues'])) {
              unset($config['settings']['leagues']);
            }
          }
          $xtwf_entity->setPluginConfig($instance_id, $config);
        }
      }
    }

    $xtwf_entity->save();
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save configuration');

    $actions['cancel'] = [
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => $this->getCancelUrl(),
        '#attributes' => [
            'class' => ['button'],
        ],
    ];

    return $actions;
  }

}
