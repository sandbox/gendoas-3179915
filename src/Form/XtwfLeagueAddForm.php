<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfLeagueAddForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for adding a league entity.
 *
 * @internal
 */
class XtwfLeagueAddForm extends XtwfLeagueFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Added Xtwf League %label.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect($this->getCollectionRoute());
    return $this->entity;
  }

}
