<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfLeagueEditForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for adding a league entity
 *
 * @internal
 */
class XtwfLeagueEditForm extends XtwfLeagueFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Xtwf League %label has been updated.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect($this->getCollectionRoute());
    return $this->entity;
  }

}
