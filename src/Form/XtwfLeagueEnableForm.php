<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfLeagueEnableForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Enable/disable confirmation form for slog toolbar.
 */
class XtwfLeagueEnableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_wfrating_league_confirm_enable';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $enable = !empty($this->entity->get('status')) ? $this->t('disable') : $this->t('enable');
    return $this->t('Are you sure you want to %enable league %title?', ['%enable' => $enable, '%title' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.xtwfleague.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('You are about to change league status.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Change status');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $start_league = $this->entity;
    $start_league_id = $start_league->id();
    $new_status = !$start_league->get('status');
    $start_found = FALSE;
    
    if ($new_status) {
      foreach (SlogXtwfRating::getXtwfLeagues(TRUE) as $id => $league) {
        if ($new_status !== (boolean) $league->get('status')) {
          $league->set('status', $new_status)->save();
        }
        
        if ($id === $start_league_id) {
          break;
        }
      }
    }
    else {
      foreach (SlogXtwfRating::getXtwfLeagues(TRUE) as $id => $league) {
        if (!$start_found) {
          $start_found = ($id === $start_league_id);
        }

        if ($start_found && ($new_status !== (boolean) $league->get('status'))) {
          $league->set('status', $new_status)->save();
        }
      }
    }
    
    
    
    $form_state->setRedirectUrl($this->getCancelUrl());

    $enabled = empty($start_league->get('status')) ? $this->t('disabled') : $this->t('enabled');
    $args = [
        '%name' => $start_league->label(),
        '%enabled' => $enabled,
    ];
    $msg = t('League %name has been %enabled.', $args);
    \Drupal::messenger()->addStatus($msg);
    SlogXtwfRating::logger()->notice($msg);
  }

}
