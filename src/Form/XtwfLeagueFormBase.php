<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfLeagueFormBase.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityForm;

/**
 * Provides a base form for a ...
 */
abstract class XtwfLeagueFormBase extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $xtwf_league = $this->entity;
    $form['label'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#default_value' => $xtwf_league->label(),
        '#required' => TRUE,
        '#weight' => -30,
    ];
    $form['id'] = [
        '#type' => 'machine_name',
        '#required' => TRUE,
        '#default_value' => $xtwf_league->id(),
        '#maxlength' => 255,
        '#machine_name' => [
            'exists' => [$this, 'exists'],
            'source' => ['label'],
        ],
        '#disabled' => !$xtwf_league->isNew(),
        '#weight' => -20,
    ];

    return parent::form($form, $form_state);
  }

  /**
   * Determines if the entity already exists.
   *
   * @param string $xtwfentity_id
   *   The entity ID
   *
   * @return bool
   *   TRUE if the entiy exists, FALSE otherwise.
   */
  public function exists($xtwfentity_id) {
    return (bool) $this->entityTypeManager
                    ->getStorage($this->getEntityType())
                    ->getQuery()
                    ->accessCheck(FALSE)
                    ->condition('id', $xtwfentity_id)
                    ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    // @todo Move trimming upstream.
    $entity_id = trim($form_state->getValue('id'));
    $entity_label = trim($form_state->getValue('label'));

    // Ensure that the values to be saved later are exactly the ones validated.
    $form_state->setValueForElement($form['id'], $entity_id);
    $form_state->setValueForElement($form['label'], $entity_label);

    $format_exists = $this->entityTypeManager
            ->getStorage($this->getEntityType())
            ->getQuery()
            ->accessCheck(FALSE)
            ->condition('id', $entity_id, '<>')
            ->condition('label', $entity_label)
            ->execute();
    if ($format_exists) {
      $form_state->setErrorByName('label', t('Entity names must be unique. A entity named %label already exists.', ['%label' => $entity_label]));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save configuration');

    $actions['cancel'] = [
        '#type' => 'link',
        '#title' => $this->t('Cancel'),
        '#url' => $this->getCancelUrl(),
        '#attributes' => [
            'class' => ['button'],
        ],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute() {
    return 'entity.xtwfleague.collection';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityType() {
    return 'xtwfleague';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute($this->getCollectionRoute());
  }

}
