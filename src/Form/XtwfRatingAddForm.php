<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfRatingAddForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for adding a ...
 *
 * @internal
 */
class XtwfRatingAddForm extends XtwfRatingFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Added Xtwf Rating %label.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect($this->getCollectionRoute());
    return $this->entity;
  }

}
