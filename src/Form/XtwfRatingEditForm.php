<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfRatingEditForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Form\FormStateInterface;
//use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form for adding a ...
 *
 * @internal
 */
class XtwfRatingEditForm extends XtwfRatingFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Xtwf Rating %label has been updated.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect($this->getCollectionRoute());
    return $this->entity;
  }

}
