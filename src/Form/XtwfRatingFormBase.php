<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfRatingFormBase.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\ContentRatingNull;
use Drupal\Core\Url;

/**
 * Provides a base form for a ...
 */
abstract class XtwfRatingFormBase extends XtwfEntityFormBase {

  protected function getEntityType() {
    return 'xtwfrating';
  }

  protected function getCollectionRoute() {
    return 'entity.xtwfrating.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute($this->getCollectionRoute());
  }

  protected function verifyWfPlugins($wf_plugins) {
    foreach ($wf_plugins as $plugin_id => $wf_plugin) {
      // When a plugin is missing, it is replaced by the null plugin. Remove it
      // here, so that saving the form will remove the missing plugin.
      if ($wf_plugin instanceof ContentRatingNull) {
        $this->messenger()->addWarning($this->t('The %plugin plugin is missing, and will be removed once this xtwf rating is saved.', ['%plugin' => $plugin_id]));
        $wf_plugins->removeInstanceID($plugin_id);
      }
    }
  }

}
