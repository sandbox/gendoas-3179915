<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfValidatorEditForm.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\Core\Form\FormStateInterface;
//use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form for adding a ...
 *
 * @internal
 */
class XtwfValidatorEditForm extends XtwfValidatorFormBase {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->messenger()->addStatus($this->t('Xtwf Validator %label has been updated.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect($this->getCollectionRoute());
    return $this->entity;
  }

}
