<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Form\XtwfValidatorFormBase.
 */

namespace Drupal\sxt_wfrating\Form;

use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\ContentValidatorNull;
use Drupal\Core\Url;

/**
 * Provides a base form for a ...
 */
abstract class XtwfValidatorFormBase extends XtwfEntityFormBase {

  protected function getEntityType() {
    return 'xtwfvalidator';
  }

  protected function getCollectionRoute() {
    return 'entity.xtwfvalidator.collection';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute($this->getCollectionRoute());
  }

  protected function verifyWfPlugins($wf_plugins) {
    foreach ($wf_plugins as $plugin_id => $wf_plugin) {
      // When a plugin is missing, it is replaced by the null plugin. Remove it
      // here, so that saving the form will remove the missing plugin.
      if ($wf_plugin instanceof ContentValidatorNull) {
        $this->messenger()->addWarning($this->t('The %plugin plugin is missing, and will be removed once this xtwf validator is saved.', ['%plugin' => $plugin_id]));
        $wf_plugins->removeInstanceID($plugin_id);
      }
    }
  }

}
