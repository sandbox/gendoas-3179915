<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\WfPluginManagerContentValidator.
 */

namespace Drupal\sxt_wfrating\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\slogxt\Plugin\DefinitionsSortedTrait;

/**
 */
class WfPluginManagerContentValidator extends DefaultPluginManager {

  use DefinitionsSortedTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $plugin_definition_annotation_name = 'Drupal\sxt_wfrating\Annotation\WfContentValidator';
    $plugin_interface = null;
    parent::__construct("Plugin/sxt_wfrating", $namespaces, $module_handler, //
            $plugin_interface, //
            $plugin_definition_annotation_name);
    $cache_key = 'sxt_wfrating:contentvalidator_plugins';
    $this->setCacheBackend($cache_backend, $cache_key, [$cache_key]);
  }

}
