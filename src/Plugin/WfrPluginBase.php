<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\WfrPluginBase.
 */

namespace Drupal\sxt_wfrating\Plugin;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\slogxt\Plugin\XtPluginBase;

abstract class WfrPluginBase extends XtPluginBase {

  /**
   * The name of the provider that owns this plugin.
   *
   * @var string
   */
  public $provider;

  /**
   * A Boolean indicating whether this plugin is enabled.
   *
   * @var bool
   */
  public $status = FALSE;

  /**
   * The weight of this plugin compared to others in a plugin collection.
   *
   * @var int
   */
  public $weight = 0;

  /**
   * An associative array containing the configured settings of this plugin.
   *
   * @var array
   */
  public $settings = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $route_provider = \Drupal::service('router.route_provider');
    parent::__construct($configuration, $plugin_id, $plugin_definition, $route_provider);

    $this->provider = $this->pluginDefinition['provider'];
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    if (!empty($configuration['settings'])) {
      $this->configuration = $configuration;
    }
    $this->status = (bool) ($configuration['status'] ?? $this->status);
    $this->weight = (integer) ($configuration['weight'] ?? $this->weight);
    $this->settings = $this->status ? (array) ($configuration['settings'] ?? []) : [];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
        'id' => $this->getPluginId(),
        'provider' => $this->pluginDefinition['provider'],
        'status' => $this->status,
        'weight' => $this->weight,
        'settings' => $this->settings,
            ] + parent::getConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'provider' => $this->pluginDefinition['provider'],
        'status' => FALSE,
        'weight' => $this->pluginDefinition['weight'] ?: 0,
        'settings' => $this->pluginDefinition['settings'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['title'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

  /**
   */
  public function getSettings() {
    return $this->getConfiguration()['settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // Implementations should work with and return $form. Returning an empty
    // array here allows the administration form to identify whether
    // the plugin has any settings form elements.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, FormStateInterface $form_state) {
    // nothing by default
  }

  /**
   * {@inheritdoc}
   */
  public function getLeagueName($league_id) {
    if ($league = SlogXtwfRating::getXtwfLeagueByIndex($league_id)) {
      return $league->labelCamelCase();
    }
    return "league-$league_id";
  }

  /**
   */
  protected function getMwInfoTopRoute() {
    return 'none';
  }

  /**
   */
  protected function getMwInfoTopLink() {
    static $target_items = FALSE;
    if (!$target_items) {
      $target_items = SlogXt::getXtTextListItems($this->getMwInfoTopRoute());
    }

    $target_id = $this->getPluginId();
    $target_item = $target_items[$target_id] ?? [];
    if ($plugin = $target_item['plugin']) {
      $xttext_target_id = $target_item['plugin']->getXtTextTargetId();
      if ($xttext_target_id) {
        return "['[XtsiMoreLink:sid|id=$xttext_target_id]']";
      }
    }

    return FALSE;
  }


  /**
   */
  public function getMwInfoContent() {
    $it = "''";
    $info[] = ';' . $this->getLabel();
    if ($top_link = $this->getMwInfoTopLink()) {
      $info[] = $top_link;
    }
    $description = $this->getDescription();
    if (!empty($description)) {
      $info[] = "*$it{$description}$it";
    }

    if ($settings_info = $this->getMwInfoSettings()) {
      $info[] = $settings_info;
    } else {
      $nosetting = t('There are no settings');
      $info[] = "*''$nosetting''";
    }

    return implode("\n", $info);
  }

  /**
   */
  public function getMwInfoSettings() {
    $settings = $this->getSettings();
    if (empty($settings)) {
      return FALSE;
    }  

    $info[] = '*' . $this->getMwInfoLine($settings);
    if ($leagues = $settings['leagues'] ?? FALSE) {
      foreach ($leagues as $league_idx => $league) {
        if ($league['override']) {
          $league_line = $this->getMwInfoLine($league);
          $league_name = $this->getLeagueName($league_idx);
          $info[] = "*<u>$league_name</u>: $league_line";
        }
      }
    }
    
    return implode("\n", $info);
  }
  
  /**
   */
  public function getMwInfoLine($settings) {
    return '...not implemented';
  }

}
