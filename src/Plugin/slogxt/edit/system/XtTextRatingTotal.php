<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\slogxt\edit\system\XtTextRatingTotal.
 */

namespace Drupal\sxt_wfrating\Plugin\slogxt\edit\system;

use Drupal\slogxt\Plugin\XtPluginEditBase;

/**
 * @SlogxtEdit(
 *   id = "slogxt_system_xttext_ratingtotal",
 *   bundle = "system",
 *   title = @Translation("XtTextRatingTotal"),
 *   description = @Translation("...not really an action, for administration of TxTxFallback only"),
 *   route_name = "slogxt.system.dummy",
 *   weight = 2
 * )
 */
class XtTextRatingTotal extends XtPluginEditBase {

  public function access() {
    return FALSE;
  }

}
