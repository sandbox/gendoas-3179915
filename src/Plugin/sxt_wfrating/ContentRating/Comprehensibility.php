<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Comprehensibility.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "comprehensibility",
 *   title = @Translation("Comprehensibility"),
 *   settings = {
 *     "labels" = @Translation("compreh01;compreh02;compreh03;compreh04;compreh05"),
 *     "description" = @Translation("The quality of being easy or possible to understand."),
 *   },
 *   weight = 0
 * )
 */
class Comprehensibility extends WfContentRatingBase {
  

  
}
