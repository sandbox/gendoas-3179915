<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Correctness.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "correctness",
 *   title = @Translation("Correctness"),
 *   settings = {
 *     "labels" = @Translation("correct01;correct02;correct03;correct04;correct05"),
 *     "description" = @Translation("The quality of being in agreement with the true facts or with what is generally accepted."),
 *   },
 *   weight = 10
 * )
 */
class Correctness extends WfContentRatingBase {

}
