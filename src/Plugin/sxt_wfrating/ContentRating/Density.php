<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Density.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "density",
 *   title = @Translation("Density"),
 *   settings = {
 *     "labels" = @Translation("density01;density02;density03;density04;density05"),
 *     "description" = @Translation("The relationship between the substance of an article and its length."),
 *   },
 *   weight = 99
 * )
 */
class Density extends WfContentRatingBase {
//Information density (Informationsdichte)
}
