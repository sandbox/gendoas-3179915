<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Netiquette.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "netiquette",
 *   title = @Translation("Netiquette"),
 *   settings = {
 *     "labels" = @Translation("netiq01;netiq02;netiq03;netiq04;netiq05"),
 *     "description" = @Translation("The set of rules about behaviour that is acceptable on the internet."),
 *   },
 *   weight = 40
 * )
 */
class Netiquette extends WfContentRatingBase {

}
