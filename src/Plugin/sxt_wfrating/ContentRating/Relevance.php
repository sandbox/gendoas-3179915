<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Relevance.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "relevance",
 *   title = @Translation("Relevance"),
 *   settings = {
 *     "labels" = @Translation("relev01;relev02;relev03;relev04;relev05"),
 *     "description" = @Translation("The degree to which something is related or useful to what is being talked about."),
 *   },
 *   weight = 20
 * )
 */
class Relevance extends WfContentRatingBase {

}
