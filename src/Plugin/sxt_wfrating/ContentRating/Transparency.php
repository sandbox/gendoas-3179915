<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\Transparency.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

/**
 * @WfContentRating(
 *   id = "transparency",
 *   title = @Translation("Transparency"),
 *   settings = {
 *     "labels" = @Translation("transp01;transp02;transp03;transp04;transp05"),
 *     "description" = @Translation("The characteristic of being easy to see through."),
 *   },
 *   weight = 30
 * )
 */
class Transparency extends WfContentRatingBase {

}
