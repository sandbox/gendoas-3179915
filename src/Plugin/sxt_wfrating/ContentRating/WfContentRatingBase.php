<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\WfContentRatingBase.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_wfrating\Plugin\WfrPluginBase;
use Drupal\sxt_wfrating\XtwfLeagueFormTrait;

abstract class WfContentRatingBase extends WfrPluginBase implements WfContentRatingInterface {

  use XtwfLeagueFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getDescriptionByLeague($league_idx) {
    $settings = $this->getLeagueSettings($league_idx);
    return $settings['description'] ?? $this->pluginDefinition['description'];
  }

  public function getRatingOptionsByLeague($league_idx, $add_na) {
    $settings = $this->getLeagueSettings($league_idx);
    $start = $add_na ? [(string) t('N/A --- not rated / not specified')] : [];
    $labels_string = (string) $settings['labels'] ?? $this->pluginDefinition['description'];
    $labels = explode(';', $labels_string);
    return array_merge($start, $labels);
  }

  /**
   * Overrides \Drupal\sxt_wfrating\Plugin\WfrPluginBase::settingsForm();
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_key = '#default_value';
    $field_labels = [
        '#type' => 'textfield',
        '#title' => t('Option labels'),
    ];
    $field_description = [
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#rows' => 2,
        '#resizable' => 'none',
    ];

    // base fields
    $settings = $this->getLeagueRealSettings(0);
    $elabels = SlogXtwfRating::getOverallRatingLabels();
    $example = t('Example');
    $form['labels'] = $field_labels + [
        $default_key => $settings['labels'],
        '#description' => t('Set option labels for rating values. Five labels separated by semicolon.') . "<br />$example: $elabels",
    ];
    $form['description'] = $field_description + [
        $default_key => $settings['description'],
        '#description' => t('Enter the help text to be displayed to the user.'),
    ];

    // leagues
    $leagues = SlogXtwfRating::getXtwfLeaguesByIndex();
    foreach ($leagues as $league_idx => $league) {
      $settings = $this->getLeagueRealSettings($league_idx);
      $league_name = $league->label();
      $override = (boolean) $settings['override'];
      $form['leagues'][$league_idx] = $this->addLeagueBaseField($league_idx, $league_name, $override);
      $form['leagues'][$league_idx]['labels'] = $field_labels + [$default_key => ($settings['labels'] ?? [])];
      $form['leagues'][$league_idx]['description'] = $field_description + [$default_key => ($settings['description'] ?? [])];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, FormStateInterface $form_state) {
    $id = $this->getPluginId();
    $all_values = $form_state->getValues();  //'wfplugins'
    $values = $all_values['wfplugins'][$id] ?? [];

    // validate enabled plugin only
    if (!empty($values['status']) && !empty($values['settings'])) {
      $title = $this->getLabel();
      $p_settings = $form['wfplugins']['settings'][$id];

      $err_msg = t('Five option labels are required, none of them to be empty.');
      $labels = array_filter(explode(';', $values['settings']['labels'] ?? ''));
      if (count($labels) !== 5) {
        $element = $p_settings['labels'];
        $message = "$title(0): " . $err_msg;
        $form_state->setErrorByName(implode('][', $element['#parents']), $message);
        return;
      }

      if (empty($values['settings']['description'])) {
        $element = $p_settings['description'];
        $required = t('Description is required.');
        $message = "$title(0): $required";
        $form_state->setErrorByName(implode('][', $element['#parents']), $message);
        return;
      }

      $leagues = $values['settings']['leagues'];
      $p_leagues = $p_settings['leagues'];
      foreach ($leagues as $league_idx => $league) {
        if (!empty($league['override'])) {
          $labels = array_filter(explode(';', $league['labels'] ?? ''));
          if (count($labels) !== 5) {
            $element = $p_leagues[$league_idx]['labels'];
            $message = "$title($league_idx): " . $err_msg;
            $form_state->setErrorByName(implode('][', $element['#parents']), $message);
            return;
          }

          if (empty($league['description'])) {
            $element = $p_leagues[$league_idx]['description'];
            $message = "$title($league_idx): " . t('You have to override description too.');
            $form_state->setErrorByName(implode('][', $element['#parents']), $message);
            return;
          }
        }
      }
    }
  }

  /**
   */
  public function getMwInfoLine($settings) {
    $it = "''";
    $labels = $settings['labels'] ?? '??';
    $description = $settings['description'] ?? '??';

    return "$labels<br />$it{$description}$it";
  }

  /**
   */
  protected function getMwInfoTopRoute() {
    return 'sxt_workflow.xttext.content_rating';
  }

}
