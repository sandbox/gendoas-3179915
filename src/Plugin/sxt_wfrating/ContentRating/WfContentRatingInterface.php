<?php

/**
 * @file
 * Definition of \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating\WfContentRatingInterface.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentRating;

use Drupal\Component\Plugin\ConfigurableInterface;

interface WfContentRatingInterface extends ConfigurableInterface {
    
}
