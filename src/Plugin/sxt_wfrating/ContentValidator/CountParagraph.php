<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\CountParagraph.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase;

/**
 * @WfContentValidator(
 *   id = "count_paragraphs",
 *   title = @Translation("Count paragraphs"),
 *   description = @Translation("Simple count of paragraphs in content (min/max)."),
 *   settings = {
 *     "min" = 3,
 *     "max" = 20,
 *     "tags" = "h2;h3",
 *   },
 *   weight = 1,
 * )
 */
class CountParagraph extends WfContentValidatorBase {

  /**
   * Overrides \Drupal\sxt_wfrating\Plugin\WfrPluginBase::settingsForm();
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_key = '#default_value';
    $field_min = [
        '#type' => 'textfield',
        '#title' => $this->t('Minimal'),
        '#description' => t('Set the required paragraphs in content.'),
        '#size' => 10,
    ];
    $field_max = [
        '#type' => 'textfield',
        '#title' => $this->t('Maximal'),
        '#description' => t('Set the maximal allowed paragraphs in content.'),
        '#size' => 10,
    ];
    $field_tags = [
        '#type' => 'textfield',
        '#title' => $this->t('Allowed tags'),
        '#description' => t('Set the allowed tags as paragraph title separated by semicolon (e.g. h2;h3). Allowed are header tags only.'),
        '#size' => 20,
    ];

    // base fields
    $settings = $this->getLeagueRealSettings(0);
    $form['min'] = $field_min + [$default_key => $settings['min']];
    $form['max'] = $field_max + [$default_key => $settings['max']];
    $form['tags'] = $field_tags + [$default_key => $settings['tags']];

    // leagues
    unset($field_min['#description']);
    unset($field_max['#description']);
    unset($field_tags['#description']);
    $leagues = SlogXtwfRating::getXtwfLeaguesByIndex();
    foreach ($leagues as $league_idx => $league) {
      $settings = $this->getLeagueRealSettings($league_idx);
      $league_name = $league->label();
      $override = (boolean) $settings['override'];
      $form['leagues'][$league_idx] = $this->addLeagueBaseField($league_idx, $league_name, $override);
      $form['leagues'][$league_idx]['min'] = $field_min + [$default_key => ($settings['min'] ?? '')];
      $form['leagues'][$league_idx]['max'] = $field_max + [$default_key => ($settings['max'] ?? '')];
      $form['leagues'][$league_idx]['tags'] = $field_tags + [$default_key => ($settings['tags'] ?? '')];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $this->removeDomArticleHeader($document);
    // get the settings valid for $league_idx
    $settings = $this->getLeagueSettings($league_idx);
    $delimiter = ';';
    $all_tags = explode($delimiter, 'h1;h2;h3;h4;h5');
    $s_tags = explode($delimiter, (string) $settings['tags']);
    $s_tags = array_filter($s_tags);
    $allowed_tags = array_intersect($all_tags, $s_tags);
    
    $num_items = 0;
    foreach ($allowed_tags as $tag) {
      $headers = $document->getElementsByTagName($tag);
      if ($headers->length) {
        $num_items += $headers->length;
      }
    }
    
    $min = (integer) $settings['min'];
    $max = (integer) $settings['max'];

    $valid = ($num_items >= $min && $num_items <= $max);
    $ok = $this->txtOk($valid);
    $args = [
        '@min' => $min,
        '@max' => $max,
        '@num' => SlogXt::htmlHighlightText($num_items, !$valid),
        '@ok' => SlogXt::htmlHighlightText($ok, !$valid),
    ];
    $msg = t('Count paragraphs: @num (min=@min, max=@max) - @ok', $args);
    
    return [
        'valid' => $valid,
        'message' => htmlspecialchars_decode($msg),
    ];
  }

  /**
   */
  public function getMwInfoLine($settings) {
    $min = $settings['min'] ?? '??';
    $max = $settings['max'] ?? '??';
    $tags = $settings['tags'] ?? '??';
    $minlabel = t('Min');
    $maxlabel = t('Max');
    $tlabel = t('Header');
    return "$minlabel: $min, $maxlabel: $max, $tlabel: $tags";
  }
  
}
