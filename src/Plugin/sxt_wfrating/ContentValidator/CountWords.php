<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\CountWords.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase;

/**
 * @WfContentValidator(
 *   id = "count_words",
 *   title = @Translation("Count words"),
 *   description = @Translation("Simple count of words in content (min/max)."),
 *   settings = {
 *     "min" = 200,
 *     "max" = 800,
 *   },
 *   weight = 0,
 * )
 */
class CountWords extends WfContentValidatorBase {

  /**
   * Overrides \Drupal\sxt_wfrating\Plugin\WfrPluginBase::settingsForm();
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_key = '#default_value';
    $field_min = [
        '#type' => 'textfield',
        '#title' => $this->t('Minimal'),
        '#description' => t('Set the required words in content.'),
        '#size' => 10,
    ];
    $field_max = [
        '#type' => 'textfield',
        '#title' => $this->t('Maximal'),
        '#description' => t('Set the maximal allowed words in content.'),
        '#size' => 10,
    ];

    // base fields
    $settings = $this->getLeagueRealSettings(0);
    $form['min'] = $field_min + [$default_key => $settings['min']];
    $form['max'] = $field_max + [$default_key => $settings['max']];

    // leagues
    unset($field_min['#description']);
    unset($field_max['#description']);
    $leagues = SlogXtwfRating::getXtwfLeaguesByIndex();
    foreach ($leagues as $league_idx => $league) {
      $settings = $this->getLeagueRealSettings($league_idx);
      $league_name = $league->label();
      $override = (boolean) $settings['override'];
      $form['leagues'][$league_idx] = $this->addLeagueBaseField($league_idx, $league_name, $override);
      $form['leagues'][$league_idx]['min'] = $field_min + [$default_key => ($settings['min'] ?? '')];
      $form['leagues'][$league_idx]['max'] = $field_max + [$default_key => ($settings['max'] ?? '')];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $this->removeDomArticleHeader($document);
    $this->removeDomMwToc($document);
    $num_words = str_word_count($document->textContent);
    // get the settings valid for $league_idx
    $settings = $this->getLeagueSettings($league_idx);
    $min = $settings['min'];
    $max = $settings['max'];

    $valid = ($num_words >= $min && $num_words <= $max);
    $ok = $this->txtOk($valid);
    $args = [
        '@min' => $min,
        '@max' => $max,
        '@num' => SlogXt::htmlHighlightText($num_words, !$valid),
        '@ok' => SlogXt::htmlHighlightText($ok, !$valid),
    ];
    $msg = t('Count words: @num (min=@min, max=@max) - @ok', $args);

    return [
        'valid' => $valid,
        'message' => htmlspecialchars_decode($msg),
    ];
  }

  /**
   */
  public function getMwInfoLine($settings) {
    $min = $settings['min'] ?? '??';
    $max = $settings['max'] ?? '??';
    $minlabel = t('Min');
    $maxlabel = t('Max');
    return "$minlabel: $min, $maxlabel: $max";
  }
  
}
