<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\DummyValidatorUnvalid.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase;

/**
 * @WfContentValidator(
 *   id = "unvalid",
 *   title = @Translation("Unvalid"),
 *   description = @Translation("Dummy validator that allways delivers unvalid."),
 *   weight = 91,
 * )
 */
class DummyValidatorUnvalid extends WfContentValidatorBase {
  
  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $valid = FALSE;
    $ok = SlogXt::htmlHighlightText($this->txtOk($valid), !$valid);
    $info = $this->getDescription();
    return [
        'valid' => $valid,
        'message' => "$info - $ok",
    ];   
  }
  
}
