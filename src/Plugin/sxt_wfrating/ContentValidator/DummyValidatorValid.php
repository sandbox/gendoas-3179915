<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\DummyValidatorValid.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase;

/**
 * @WfContentValidator(
 *   id = "valid",
 *   title = @Translation("Valid"),
 *   description = @Translation("Dummy validator that allways delivers valid."),
 *   weight = 90,
 * )
 */
class DummyValidatorValid extends WfContentValidatorBase {

  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $valid = TRUE;
    $ok = SlogXt::htmlHighlightText($this->txtOk($valid), !$valid);
    $info = $this->getDescription();
    return [
        'valid' => $valid,
        'message' => "$info - $ok",
    ];
  }

}
