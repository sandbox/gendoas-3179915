<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\RequiredParagraph.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase;

/**
 * @WfContentValidator(
 *   id = "required_paragraphs",
 *   title = @Translation("Required paragraphs"),
 *   description = @Translation("Validate that the required paragraphs are existing in content."),
 *   settings = {
 *     "required" = "Abstract;Methods;Results",
 *     "tags" = "h2;h3",
 *   },
 *   weight = 10,
 * )
 */
class RequiredParagraph extends WfContentValidatorBase {

  /**
   * Overrides \Drupal\sxt_wfrating\Plugin\WfrPluginBase::settingsForm();
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $default_key = '#default_value';
    $field_required = [
        '#type' => 'textfield',
        '#title' => $this->t('Required paragraphs'),
        '#description' => t('Set the required paragraph titles separated by semicolon (e.g. Abstract;Methods;Results).'),
    ];
    $field_tags = [
        '#type' => 'textfield',
        '#title' => $this->t('Allowed tags'),
        '#description' => t('Set the allowed tags as paragraph title separated by semicolon (e.g. h2;h3). Allowed are header tags only.'),
        '#size' => 20,
    ];

    // base fields
    $settings = $this->getLeagueRealSettings(0);
    $form['required'] = $field_required + [$default_key => $settings['required']];
    $form['tags'] = $field_tags + [$default_key => $settings['tags']];

    // leagues
    unset($field_required['#description']);
    unset($field_tags['#description']);
    $leagues = SlogXtwfRating::getXtwfLeaguesByIndex();
    foreach ($leagues as $league_idx => $league) {
      $settings = $this->getLeagueRealSettings($league_idx);
      $league_name = $league->label();
      $override = (boolean) $settings['override'];
      $form['leagues'][$league_idx] = $this->addLeagueBaseField($league_idx, $league_name, $override);
      $form['leagues'][$league_idx]['required'] = $field_required + [$default_key => ($settings['required'] ?? '')];
      $form['leagues'][$league_idx]['tags'] = $field_tags + [$default_key => ($settings['tags'] ?? '')];
    }
    
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $this->removeDomArticleHeader($document);
    // get the settings valid for $league_idx
    $settings = $this->getLeagueSettings($league_idx);
    $delimiter = ';';
    $all_tags = explode($delimiter, 'h1;h2;h3;h4;h5');
    $s_tags = explode($delimiter, (string) $settings['tags']);
    $s_tags = array_filter($s_tags);
    $allowed_tags = array_intersect($all_tags, $s_tags);

    foreach ($allowed_tags as $tag) {
      $headers = $document->getElementsByTagName($tag);
      if ($headers->length) {
        break;
      }
    }

    $arequired = explode($delimiter, (string) $settings['required']);
    $arequired = array_filter($arequired);
    $required = array_map('trim', $arequired);
    $done = [];
    if ($headers->length) {
      foreach ($headers as $header) {
        $h_text = trim($header->textContent);
        if (!in_array($h_text, $done) && in_array($h_text, $required)) {
          $done[] = $h_text;
        }
      }
    }

    $missing = array_diff($required, $done);
    $valid = empty($missing);
    $ok = $this->txtOk($valid);
    $txt_missing = implode('; ', $missing);
    $args = [
        '@required' => implode('; ', $required),
        '@missing' => SlogXt::htmlHighlightText($txt_missing),
        '@ok' => SlogXt::htmlHighlightText($ok, !$valid),
    ];
    if ($valid) {
      $msg = t('Required paragraphs: @required - @ok', $args);
    }
    else {
      $msg = t('Missing required paragraphs: @missing - @ok', $args);
    }

    return [
        'valid' => $valid,
        'message' => htmlspecialchars_decode($msg),
    ];
  }

  /**
   */
  public function getMwInfoLine($settings) {
    $required = $settings['required'] ?? '??';
    $tags = $settings['tags'] ?? '??';
    $rlabel = t('Required');
    $tlabel = t('Header');
    return "$rlabel: $required, $tlabel: $tags";
  }
  
}
