<?php

/**
 * @file
 * Definition of Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorBase.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sxt_wfrating\XtwfLeagueFormTrait;
use Drupal\sxt_wfrating\Plugin\WfrPluginBase;

abstract class WfContentValidatorBase extends WfrPluginBase implements WfContentValidatorInterface {

  use XtwfLeagueFormTrait;

  /**
   * Overrides \Drupal\sxt_wfrating\Plugin\WfrPluginBase::settingsForm();
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(\DOMDocument $document, $league_idx) {
    $valid = FALSE;
    $msg = t('.....not implemented: @label', ['@label' => (string) $this->getLabel()]);

    return [
        'valid' => $valid,
        'message' => $msg,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getDomArticleHeader(\DOMDocument $document) {
    if ($article = $document->getElementsByTagName('article')->item(0)) {
      if ($article->hasChildNodes()) {
        foreach ($article->childNodes as $node) {
          if ($node->tagName && $node->tagName === 'header') {
            return $node;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeDomArticleHeader(\DOMDocument $document) {
    if ($header = $this->getDomArticleHeader($document)) {
      $header->parentNode->removeChild($header);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeDomMwToc(\DOMDocument $document) {
    if ($toc = $document->getElementById('toc')) {
      $toc->parentNode->removeChild($toc);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function txtOk($valid) {
    return $valid ? t('ok') : t('insufficient');
  }

  /**
   */
  protected function getMwInfoTopRoute() {
    return 'sxt_workflow.xttext.content_validator';
  }

}
