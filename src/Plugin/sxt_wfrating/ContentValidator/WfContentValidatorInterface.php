<?php

/**
 * @file
 * Definition of \Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator\WfContentValidatorInterface.
 */

namespace Drupal\sxt_wfrating\Plugin\sxt_wfrating\ContentValidator;

use Drupal\Component\Plugin\ConfigurableInterface;

interface WfContentValidatorInterface extends ConfigurableInterface {
  
  /**
   * Whether the content meets the requirements.
   * 
   * - Validation by league.
   * 
   * @param \DOMDocument $document
   * @param type $league_idx
   * @return array 
   *  Array contains (boolean) valid and (string) message
   */
  public function validate(\DOMDocument $document, $league_idx);
    
}
