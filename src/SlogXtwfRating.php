<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\SlogXtwfRating.
 */

namespace Drupal\sxt_wfrating;

use Drupal\slogxt\SlogXt;
use Drupal\sxt_wfrating\Event\XtwfRatingEvents;
use Drupal\slogxt\Event\SlogxtGenericEvent;

/**
 */
class SlogXtwfRating {

  protected static $storage = [];
  protected static $xtwfEntities = [];

  /**
   * Return entity storage for entity type.
   * 
   * @param string $entity_type_id
   * @return \Drupal\Core\Entity\EntityStorageInterface
   */
  public static function getStorage($entity_type_id) {
    if (!isset(self::$storage[$entity_type_id])) {
      self::$storage[$entity_type_id] = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    }

    return self::$storage[$entity_type_id];
  }

  /**
   * {@inheritdoc}
   */
  public static function getEntityType($entity_type_id) {
    return \Drupal::entityTypeManager()->getDefinition($entity_type_id);
  }

  /**
   * Return all entities if entity type.
   * 
   * @return array
   */
  public static function getXtwfEntities($entity_type_id) {
    if (!isset(self::$xtwfEntities[$entity_type_id])) {
      $entity_type = self::getEntityType($entity_type_id);
      $storage = self::getStorage($entity_type_id);
      $entity_ids = $storage->getQuery()
              ->accessCheck(FALSE)
              ->execute();
      $entities = $storage->loadMultipleOverrideFree($entity_ids);
      uasort($entities, [$entity_type->getClass(), 'sort']);

      self::$xtwfEntities[$entity_type_id] = $entities;
    }

    return self::$xtwfEntities[$entity_type_id];
  }

  public static function getXtwfRatings() {
    return self::getXtwfEntities('xtwfrating');
  }

  public static function getXtwfRating($xtwfrating_id) {
    if (!empty($xtwfrating_id)) {
      $xtwfratings = self::getXtwfRatings();
      return $xtwfratings[$xtwfrating_id] ?? FALSE;
    }
    return FALSE;
  }

  public static function getXtwfRatingOptions() {
    $options = [];
    foreach (self::getXtwfRatings() as $id => $xtwfratings) {
      $options[$id] = $xtwfratings->label();
    }
    return $options;
  }

  public static function getXtwfValidators() {
    return self::getXtwfEntities('xtwfvalidator');
  }

  public static function getXtwfValidator($xtwfvalidator_id) {
    if (!empty($xtwfvalidator_id)) {
      $xtwfvalidators = self::getXtwfValidators();
      return $xtwfvalidators[$xtwfvalidator_id] ?? FALSE;
    }
    return FALSE;
  }

  public static function getXtwfValidatorOptions() {
    $options = [];
    foreach (self::getXtwfValidators() as $id => $xtwfvalidator) {
      $options[$id] = $xtwfvalidator->label();
    }
    return $options;
  }

  public static function getXtwfLeagues($all = FALSE) {
    $xtwf_leagues = self::getXtwfEntities('xtwfleague');
    $index = 0;
    foreach ($xtwf_leagues as $xtwf_league) {
      if ($xtwf_league->isEnabled()) {
        $xtwf_league->setLeagueIndex($index++);
      }
    }

    if (!$all) {
      // enabled only
      $xtwf_leagues = array_filter($xtwf_leagues, function ($xtwf_league) {
        return $xtwf_league->isEnabled();
      });
    }

    return $xtwf_leagues;
  }

  public static function hasMultipleXtwfLeagues() {
    $leagues = self::getXtwfLeagues();
    return (count($leagues) > 1);
  }

  public static function getXtwfLeaguesByIndex($all = FALSE, $start_idx = 1) {
    $leagues_by_index = [];
    foreach (self::getXtwfLeagues() as $id => $league) {
      $league_idx = $league->getLeagueIndex();
      if ($all || $league_idx >= $start_idx) {
        $leagues_by_index[$league_idx] = $league;
      }
    }

    return $leagues_by_index;
  }

  public static function getXtwfLeagueByIndex($index) {
    $leagues_by_index = self::getXtwfLeaguesByIndex(FALSE, 0);
    return $leagues_by_index[$index] ?? FALSE;
  }

  public static function getXtTextRatingTotalTargetId() {
    $manager = SlogXt::pluginManager('edit');
    $definition = $manager->getDefinition('slogxt_system_xttext_ratingtotal');
    $plugin = $manager->createInstance($definition['id'], $definition);

    if ($target_id = (integer) $plugin->getXtTextTargetId()) {
      return $target_id;
    }
    return SlogXt::getXtTextNotFoundTargetId();
  }

  /**
   * Return the manager for content rating plugins.
   * 
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   */
  public static function contentRatingManager() {
    return \Drupal::service('plugin.manager.sxt_wfrating.contentrating');
  }

  public static function getOverallRatingLabels() {
    $opts = [
        (string) t('Insufficient'),
        (string) t('Unsatisfying'),
        (string) t('Satisfying'),
        (string) t('Fine'),
        (string) t('Excellent'),
    ];
    return implode(';', $opts);
  }

  public static function getOverallRatingDescription($is_for_pending = FALSE) {
    $hint = $is_for_pending ? '' : t('You can suspend rating to continue later.');
    return t('The overall rating of the content is required.') . " $hint";
  }

  /**
   * Return the manager for content validator plugins.
   * 
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   */
  public static function contentValidatorManager() {
    return \Drupal::service('plugin.manager.sxt_wfrating.contentvalidator');
  }

  /**
   * {@inheritdoc}
   */
  public static function canDeleteLeague($league_idx) {
    $can_delete = TRUE;
    $event = new SlogxtGenericEvent([
        'league_idx' => $league_idx,
        'can_delete' => &$can_delete,
    ]);
    \Drupal::service('event_dispatcher')
            ->dispatch($event, XtwfRatingEvents::SXT_WFRATING_CAN_DELETE_LEAGUE);

    return $can_delete;
  }

  /**
   */
  public static function logger() {
    return \Drupal::logger('sxt_wfrating');
  }

}
