<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\WfPluginCollectionBase.
 */

namespace Drupal\sxt_wfrating;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\DefaultLazyPluginCollection;

/**
 * A collection of wfratings.
 */
class WfPluginCollectionBase extends DefaultLazyPluginCollection {

  /**
   * All possible plugin IDs.
   *
   * @var array
   */
  protected $definitions;

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\sxt_wfrating\Plugin\WfContentRatingInterface
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   */
  public function getEnabledLabels() {
    $labels = [];
    foreach ($this->getEnabled() as $plugin_id => $plugin) {
      $labels[$plugin_id] = (string) $plugin->getLabel();
    }
    return $labels;
  }

  /**
   */
  public function getEnabled() {
    $enabled = [];
    foreach ($this->getAll() as $plugin_id => $plugin) {
      if ($plugin->get('status')) {
        $enabled[$plugin_id] = $plugin;
      }
    }
    return $enabled;
  }

  /**
   * Retrieves wfrating definitions and creates an instance for each wfrating.
   */
  public function getAll() {
    // Retrieve all available plugin definitions.
    if (!$this->definitions) {
      $this->definitions = $this->manager->getDefinitions();
    }

    // Ensure that there is an instance of all available plugins.
    foreach ($this->definitions as $plugin_id => $definition) {
      if (!isset($this->pluginInstances[$plugin_id])) {
        $this->initializePlugin($plugin_id);
      }
    }

    // removeInstanceId without definition
    $conf_instance_ids = array_keys($this->configurations);
    $def_instance_ids = array_keys($this->definitions);
    $diff_ids = array_diff($conf_instance_ids, $def_instance_ids);
    foreach ($diff_ids as $instance_id) {
      $this->removeInstanceId($instance_id);
    }


    return $this->pluginInstances;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id) {
    // WfRatings have a 1:1 relationship to text formats and can be added and
    // instantiated at any time.
    $configuration = $this->manager->getDefinition($instance_id);
    // Merge the actual configuration into the default configuration.
    if (isset($this->configurations[$instance_id])) {
      $configuration = NestedArray::mergeDeep($configuration, $this->configurations[$instance_id]);
    }
    $this->configurations[$instance_id] = $configuration;
    parent::initializePlugin($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  public function sortHelper($aID, $bID) {
    $a = $this->get($aID);
    $b = $this->get($bID);
    if ($a->status != $b->status) {
      return !empty($a->status) ? -1 : 1;
    }
    if ($a->weight != $b->weight) {
      return $a->weight < $b->weight ? -1 : 1;
    }
    if ($a->provider != $b->provider) {
      return strnatcasecmp($a->provider, $b->provider);
    }
    return parent::sortHelper($aID, $bID);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = parent::getConfiguration();
    // Remove configuration if it matches the defaults. In self::getAll(), we
    // load all available plugins, in addition to the enabled stored in
    // configuration. In order to prevent those from bleeding through to the
    // stored configuration, remove all plugins that match the default values.
    // Because plugins are disabled by default, this will never remove the
    // configuration of an enabled plugin.
    foreach ($configuration as $instance_id => $instance_config) {
      $default_config = [];
      $default_config['id'] = $instance_id;
      $default_config += $this->get($instance_id)->defaultConfiguration();
      if ($default_config === $instance_config) {
        unset($configuration[$instance_id]);
      }
    }
    return $configuration;
  }

  /**
   */
  public function getInstanceIds() {
    return $this->instanceIds;
  }
  
}
