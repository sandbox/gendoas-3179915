<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\XtwfEntityListBuilder.
 */

namespace Drupal\sxt_wfrating;

use Drupal\slogxt\SlogXt;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 */
class XtwfEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    SlogXt::setAdminCompactTasks();
    $header['title'] = t('Name');
    $header['items'] = t('Items');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];
    $labels = $entity->getPluginsAll()->getEnabledLabels();
    if (empty($labels)) {
      $labels = ['-'];
    }
    $row['items'] = [
      'data' => implode(', ', $labels),
    ];
    return $row + parent::buildRow($entity);
  }

}
