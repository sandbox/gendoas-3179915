<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\XtwfLeagueFormTrait.
 */

namespace Drupal\sxt_wfrating;

/**
 * A Trait for ...
 */
trait XtwfLeagueFormTrait {

  protected static $league_trait_settings = NULL;

  /**
   */
  public static function cleanupLeagues($leagues) {
    if (is_array($leagues)) {
      foreach ($leagues as $key => &$league) {
        $league['override'] = (boolean) $league['override'];
        if (!$league['override']) {
          unset($leagues[$key]);
        }
      }
    }

    return $leagues;
  }

  /**
   */
  public static function getLeagueTraitSettings($required = TRUE) {
    if ($required && !isset(self::$league_trait_settings)) {
      $message = t('Required settings not got.');
      throw new \Exception($message);
    }
    return self::$league_trait_settings;
  }

  /**
   */
  public static function setLeagueTraitSettings($settings) {
    self::$league_trait_settings = $settings;
  }

  /**
   */
  public static function getSeverityValueList($severity) {
    $leagues = $severity['leagues'] ?? [];
    unset($severity['leagues']);
    $value_items = [];
    $tmp_items = [];
    foreach ($severity as $id => $value) {
      $svalue = is_bool($value) ? ($value ? 'TRUE' : 'FALSE') : (is_array($value) ? 'array' : ($value ?: 'none'));
      $tmp_items[] = "$id: $svalue";
    }
    $value_items[] = implode(', ', $tmp_items);

    if (!empty($leagues)) {
      foreach ($leagues as $league_idx => $league) {
        if ($league['override']) {
          unset($league['override']);
          $tmp_items = [];
          foreach ($league as $id => $value) {
            $svalue = is_bool($value) ? ($value ? 'TRUE' : 'FALSE') : (is_array($value) ? 'array' : ($value ?: 'none'));
            $tmp_items[] = "$id: $svalue";
          }
          $league_entity = SlogXtwfRating::getXtwfLeagueByIndex($league_idx);
          $league_name = $league_entity->labelCamelCase();
          $value_items[] = "$league_name: " . implode(', ', $tmp_items);
        }
      }
    }

    $value_list = [
        '#theme' => 'item_list',
        '#items' => $value_items,
        '#context' => ['list_style' => 'comma-list'],
    ];
    if (count($value_items) > 1) {
      unset($value_list['#context']);
    }

    return $value_list;
  }

  /**
   */
  public static function _getLeagueSettings(int $league_idx) {
    $settings = self::getLeagueTraitSettings();
    for ($lid = $league_idx; $lid > 0; $lid--) {
      $settings = self::_getLeagueRealSettings($lid);
      if (!empty($settings['override'])) {
        return $settings;
      }
    }

    return self::_getLeagueRealSettings(0);
  }

  /**
   */
  public static function _getLeagueRealSettings(int $league_idx) {
    $settings = self::getLeagueTraitSettings();
    if ($league_idx === 0) {
      unset($settings['leagues']);
      return $settings;
    }

    // $league_idx > 0
    $leagues = $settings['leagues'] ?? [];
    $league_settings = ($leagues[$league_idx] ?? []) + ['override' => FALSE];
    return $league_settings;
  }

  /**
   */
  public static function _addLeagueBaseField(int $league_idx, $league_label, $override) {
    $element = [
        '#type' => 'details',
        '#title' => t('@lid: @llabel', ['@lid' => $league_idx, '@llabel' => $league_label]),
        '#open' => $override,
    ];
    $element['override'] = [
        '#type' => 'checkbox',
        '#title' => t('Override settings'),
        '#default_value' => $override,
    ];

    return $element;
  }

  /**
   */
  public function getLeagueSettings(int $league_idx) {
    self::setLeagueTraitSettings($this->configuration['settings']);
    return self::_getLeagueSettings($league_idx);
  }

  /**
   */
  public function getLeagueRealSettings(int $league_idx) {
    self::setLeagueTraitSettings($this->configuration['settings']);
    return self::_getLeagueRealSettings($league_idx);
  }

  /**
   */
  public function addLeagueBaseField(int $league_idx, $league_label, $override) {
    return self::_addLeagueBaseField($league_idx, $league_label, $override);
  }

}
