<?php

/**
 * @file
 * Contains \Drupal\sxt_wfrating\XtwfLeagueListBuilder.
 */

namespace Drupal\sxt_wfrating;

use Drupal\sxt_wfrating\SlogXtwfRating;
use Drupal\slogxt\SlogXt;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines a class to build a listing of league entities.
 */
class XtwfLeagueListBuilder extends DraggableListBuilder {

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigEntityListBuilder::load()
   */
  public function load() {
    return SlogXtwfRating::getXtwfLeagues(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'sxt_wfrating_overview_leagues';
  }

  /**
   * Overrides \Drupal\Core\Config\Entity\DraggableListBuilder::buildHeader()
   */
  public function buildHeader() {
    SlogXt::setAdminCompactTasks();
    $header = [
        'label' => t('Label'),
        'league_idx' => t('Index'),
        'status' => t('Status'),
    ];
    return $header + parent::buildHeader();
  }

  /**
   * Overrides \Drupal\Core\Config\Entity\DraggableListBuilder::buildRow()
   */
  public function buildRow(EntityInterface $league) {
    $enabled = $league->isEnabled();
    $txt_index = $enabled ? (string) $league->getLeagueIndex() : '';
    $txt_enabled = $enabled ? t('ok') : t('disabled');
    $row['label'] = $league->label();
    $row['league_idx'] = ['#markup' => $txt_index];
    $row['status'] = ['#markup' => $txt_enabled];
    return $row + parent::buildRow($league);
  }

  /**
   * Overrides \Drupal\Core\Config\Entity\ConfigEntityListBuilder::getDefaultOperations()
   */
  public function getDefaultOperations(EntityInterface $league) {
    $operations = parent::getDefaultOperations($league);
    if ($league->access('delete') && $league->hasLinkTemplate('enable-form')) {
      $enabled = $league->isEnabled();
      $title = $enabled ? t('Disable') : t('Enable');
      $weight = $enabled ? 40 : -40;
      $operations['status'] = [
          'title' => $title,
          'weight' => $weight,
          'url' => $this->ensureDestination($league->toUrl('enable-form')),
      ];
    }

    return $operations;
  }

}
